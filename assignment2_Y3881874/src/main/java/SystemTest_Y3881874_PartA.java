import io.morphean.msc.System_Y3881874;

public class SystemTest_Y3881874_PartA {
    System_Y3881874 system;

    public static void main(String[] args) {
        System_Y3881874 system = new System_Y3881874(
                "Dell",
                "PowerEdge",
                4);

        system.setHardDisk(20);
        system.setMemory(1024);
        system.setPurchaseCost(100);

        // Part A
        system.displayDetails();
        system.diagnoseSystem();
    }
}
