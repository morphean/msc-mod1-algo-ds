package io.morphean.msc;

public interface ISystemDetails {
    String getMake();
    String getModel();
    int getProcessorSpeed();

    void setMemory(int memory);
    void setHardDisk(double hardDiskSize);
    void setPurchaseCost(double purchaseCost);

    void displayDetails();
    String checkHDStatus();
    boolean goodMemorySize();
    void diagnoseSystem();
}
