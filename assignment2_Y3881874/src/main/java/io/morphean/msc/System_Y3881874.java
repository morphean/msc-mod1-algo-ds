package io.morphean.msc;

import java.util.Scanner;

public class System_Y3881874 implements ISystemDetails {

    private double _purchaseCost;

    public void setPurchaseCost(double purchaseCost) {
        _purchaseCost = purchaseCost;
    }

    private double _memorySize; // in MB

    public void setMemory(int memorySize) {
        if (_memorySize != memorySize) {
            _memorySize = memorySize;
        }
    }

    private double _hardDiskSize; // in GB

    public void setHardDisk(double hardDiskSize) {
        _hardDiskSize = hardDiskSize;
    }

    private String _make;

    public String getMake() {
        return _make;
    }

    private String _model;

    public String getModel() {
        return _model;
    }

    private int _speed;

    public int getProcessorSpeed() {
        return _speed;
    }

    public void displayDetails() {
        System.out.println("**** System ****");
        System.out.println();
        System.out.println("Make: " + getMake());
        System.out.println("Model: " + getModel());
        System.out.println("CPU Speed: " + getProcessorSpeed());
        System.out.println();
    }

    public String checkHDStatus() {
        return _hardDiskSize < 2.0 ? "Low" : "OK";
    }

    public boolean goodMemorySize() {
        return _memorySize > 128;
    }

    public void diagnoseSystem() {
        System.out.println("**** Memory / Storage ****");
        System.out.println();
        System.out.println("Hard Disk size = " + checkHDStatus());
        System.out.println("Memory Size OK = " + goodMemorySize());
        System.out.println();

    }

    public void displaySystemProperties() {
        String[] keys = new String[]{
                "os.arch",
                "os.name",
                "os.version",
                "user.name",
                "java.version"
        };

        SystemProperties properties = new SystemProperties(
                System.getProperty(keys[0]),
                System.getProperty(keys[1]),
                System.getProperty(keys[2]),
                System.getProperty(keys[3]),
                System.getProperty(keys[4])
        );

        System.out.println("**** Operating System ****");
        System.out.println();
        properties.print();
        System.out.println();

        switch (properties.getOsName()) {
            case SystemProperties.WINDOWS:
                if (properties.getOsVersion() == "10") {
                    System.out.println("Now that's a nice OS");
                }
                break;
            case SystemProperties.LINUX:
                System.out.println("Err.. where's the gui gone?");
                break;
            default:
                System.out.println("Weather is nice today. Isn't it, Dave?.");
                break;
        }
        System.out.println();
        System.out.println("**** Operating System ****");
    }

    public System_Y3881874(String make, String model, int speed) {
        this._make = make;
        this._model = model;
        this._speed = speed;
    }

    public void diagnoseUserSystem(System_Y3881874 system) {

        boolean captureInput = true;
        int userChoice;
        Scanner sc = new Scanner(System.in);

        do {
            printMenu();
            userChoice = sc.nextInt();

            switch (userChoice) {
                case 1:
                    displayDetails();
                    displaySystemProperties();
                    break;
                case 2:
                    diagnoseSystem();
                    break;
                case 3:
                    setDetails(system);
                    break;
                case 4:
                    captureInput = false;
                    quitProgram();
                    break;
                default:
                    print("Please try again using one of the options above.");
                    break;
            }
        }

        while (captureInput);
    }

    private void setDetails(System_Y3881874 system) {
        Scanner sc = new Scanner(System.in);

        double hd;

        do {
            print("Please enter hard disc size (in GB):");
            hd = sc.nextDouble();
            if (hd > 0) {
                system.setHardDisk(hd);
            } else {
                print("Please enter a number greater than 0");
                hd = 0;
            }
        } while (hd <= 0);

        int memory;

        do {
            print("Please enter memory size (in GB):");
            memory = sc.nextInt();
            if (memory > 0) {
                system.setMemory(memory);
            } else {
                print("Please enter a number greater than 0");
                memory = 0;
            }
        } while (memory <= 0);

        double cost;

        do {
            print("Please enter how much it cost:");
            cost = sc.nextDouble();
            if (cost > 0) {
                system.setPurchaseCost(cost);
            } else {
                print("Please enter a number greater than 0");
                cost = 0;
            }
        } while (cost <= 0);

        system.setMemory(memory);
        system.setPurchaseCost(cost);
    }

    private void quitProgram() {
        print("Quitting...");
        System.exit(0);
    }

    private void printMenu() {
        print("**** System Diagnosis ****");
        print("");
        print("Please choose from the following: ");
        print("");
        print("1. Print System Details");
        print("2. Diagnose System");
        print("3. Set Details (Memory, Storage, Cost)");
        print("4. Quit the program");
        print("");
        print("Enter your choice below (1-4):");
    }

    private void print(String msg) {
        System.out.println(msg);
    }

}
