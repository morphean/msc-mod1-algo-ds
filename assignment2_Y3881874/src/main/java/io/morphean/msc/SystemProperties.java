package io.morphean.msc;

import com.sun.org.apache.bcel.internal.generic.DCONST;

public class SystemProperties {

    public static final String WINDOWS = "Windows";
    public static final String MAC_OS_X = "Mac OS X";
    public static final String LINUX = "Linux";

    public String getJavaVersion() {
        return javaVersion;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public String getOsArch() {
        return osArch;
    }

    public String getOsName() {
        return osName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    private String javaVersion;
    private String userAccount;
    private String osArch;
    private String osName;
    private String osVersion;

    public SystemProperties(
            String osArch,
            String osName,
            String osVersion,
            String javaVersion,
            String userAccount
    ) {
        this.osArch = osArch;
        this.osName = osName;
        this.osVersion = osVersion;
        this.javaVersion = javaVersion;
        this.userAccount = userAccount;
    }

    @Override
    public String toString() {
        return "SystemProperties{" +
                "javaVersion='" + javaVersion + '\'' +
                ", userAccount='" + userAccount + '\'' +
                ", osArch='" + osArch + '\'' +
                ", osName='" + osName + '\'' +
                ", osVersion='" + osVersion + '\'' +
                '}';
    }

    public void print() {
        System.out.println("OS Architecture: "+getOsArch());
        System.out.println("OS Name: "+getOsName());
        System.out.println("OS Version: "+getOsVersion());
        System.out.println("User Account Name: "+getJavaVersion());
        System.out.println("Java Version: "+getUserAccount());
    }
}
