package io.morphean.msc;

import org.junit.rules.ExternalResource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class SysOutRule extends ExternalResource {
    private PrintStream sysOut;
    private final ByteArrayOutputStream consoleContent = new ByteArrayOutputStream();

    @Override
    protected void before() throws Throwable {
        sysOut = System.out;
        System.setOut( new PrintStream(consoleContent));
    }

    @Override
    protected void after() {
        System.setOut(sysOut);
    }

    public String asString() {
        return consoleContent.toString();
    }
}
