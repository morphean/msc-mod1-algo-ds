import io.morphean.msc.System_Y3881874;

public class SystemTest_Y3881874_PartC {
    System_Y3881874 system;

    public static void main(String[] args) {
        System_Y3881874 system = new System_Y3881874(
                "Dell",
                "PowerEdge",
                4);

        // Part C
        system.diagnoseUserSystem(system);
    }
}
