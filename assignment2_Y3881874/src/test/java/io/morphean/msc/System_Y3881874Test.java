package io.morphean.msc;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class System_Y3881874Test {

    System_Y3881874 system;

    @Before
    public void setUp() throws Exception {
        system = new System_Y3881874(
                "dell",
                "inspiron",
                2
        );
    }

    @Rule
    public SysOutRule sysOut = new SysOutRule();

    @Test
    public void setPurchaseCost() {
        system.setPurchaseCost(22.5);
        system.displayDetails();
//        assertTrue(system.displayDetails()
    }

    @Test
    public void setMemory() {
        system.setMemory(64);
        assertFalse(system.goodMemorySize());
        system.setMemory(192);
        assertTrue(system.goodMemorySize());
    }

    @Test
    public void setHardDisk() {
        system.setHardDisk(1.5);
        assertEquals(system.checkHDStatus(), "Low");
        system.setHardDisk(5);
        assertSame("OK", system.checkHDStatus());
    }

    @Test
    public void getMake() {
        assertTrue(system.getMake() == "dell");
    }

    @Test
    public void getModel() {
        assertTrue(system.getModel() == "inspiron");
    }

    @Test
    public void getProcessorSpeed() {
        assertEquals(system.getProcessorSpeed(), 2);
    }

    @Test
    public void displayDetails() {

    }

    @Test
    public void checkHDStatus() {
        system.setHardDisk(1.5);
        assertEquals(system.checkHDStatus(), "Low");
        system.setHardDisk(5);
        assertEquals(system.checkHDStatus(), "OK");

    }

    @Test
    public void goodMemorySize() {
        system.setMemory(64);
        assertFalse(system.goodMemorySize());
        system.setMemory(192);
        assertTrue(system.goodMemorySize());
    }

    @Test
    public void diagnoseSystem() {
    }

    @Test
    public void displaySystemProperties() {
    }
}