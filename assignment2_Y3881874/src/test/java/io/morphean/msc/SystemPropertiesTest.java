package io.morphean.msc;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SystemPropertiesTest {

    SystemProperties sysProps;

    @Before
    public void setUp() throws Exception {
        sysProps = new SystemProperties(
                "x86",
                "Windows",
                "7",
                "1.8u205",
                "myAccount");
    }

    @Test
    public void getJavaVersion() {
        assertTrue(sysProps.getJavaVersion() == "1.8u205");
    }

    @Test
    public void getUserAccount() {
        assertTrue(sysProps.getUserAccount() == "myAccount");
    }

    @Test
    public void getOsArch() {
        assertTrue(sysProps.getOsArch() == "x86");
    }

    @Test
    public void getOsName() {
        assertTrue(sysProps.getOsName() == "Windows");
    }

    @Test
    public void getOsVersion() {
        assertTrue(sysProps.getOsVersion() == "7");
    }
}