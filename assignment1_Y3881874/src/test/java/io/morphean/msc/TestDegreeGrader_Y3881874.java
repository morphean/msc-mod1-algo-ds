package io.morphean.msc;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestDegreeGrader_Y3881874 {

    private DegreeGrader_Y3881874 grader;

    @Before
    public void setup() {
        grader = new DegreeGrader_Y3881874();
    }

    @Test
    public void testGradeDegree(){
        assertEquals(grader.gradeDegree(75, 84, 0,0), grader.DISTINCTION);
        assertEquals(grader.gradeDegree(65, 65, 0,0), grader.MERIT);
        assertEquals(grader.gradeDegree(55, 55, 0,0), grader.PASS);
    }

    @Test
    public void isDistinctionTest() {
        Degree degree = new Degree(75, 84, 0, 0);
        assertTrue(grader.isDistinction(degree));;

        Degree degree2 = new Degree(65, 24, 5, 2);
        assertFalse(grader.isDistinction(degree2));;
    };

    @Test
    public void isMeritTest() {

        Degree degree = new Degree(65, 64, 15, 0);
        assertTrue(grader.isMerit(degree));

        Degree degree2 = new Degree(55, 54, 3, 2);
        assertFalse(grader.isMerit(degree2));
    }

    @Test
    public void isPassTest() {
        Degree degree = new Degree(52, 52, 15, 0);
        assertTrue(grader.isPass(degree));

        Degree degree2 = new Degree(65, 64, 15, 0);
        assertTrue(grader.isMerit(degree2));
    }

    @Test
    public void isFailTest() {
        Degree degree = new Degree(48, 48, 25, 2);
        assertTrue(grader.isFail(degree));

        Degree degree2 = new Degree(65, 64, 15, 0);
        assertFalse(grader.isFail(degree2));
    }

    @Test
    public void validateInputsTest() {
        assertTrue(grader.validateModuleAverage(10));
        assertTrue(grader.validateModuleAverage(80));
        assertFalse(grader.validateModuleAverage(120));
        assertFalse(grader.validateModuleAverage(-5));

        assertTrue(grader.validateCompensatableFailedCredits(10));
        assertTrue(grader.validateCompensatableFailedCredits(80));
        assertFalse(grader.validateCompensatableFailedCredits(190));
        assertFalse(grader.validateCompensatableFailedCredits(-5));

        assertTrue(grader.validateNumberOfFailedModules(10));
        assertTrue(grader.validateNumberOfFailedModules(2));
        assertFalse(grader.validateNumberOfFailedModules(15));
        assertFalse(grader.validateNumberOfFailedModules(-5));
    }

}
