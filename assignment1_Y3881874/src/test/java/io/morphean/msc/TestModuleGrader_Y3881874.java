package io.morphean.msc;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestModuleGrader_Y3881874 {

    private ModuleGrader_Y3881874 grader;

    @Before
    public void setup() {
        grader = new ModuleGrader_Y3881874();
    }

    @Test
    public void gradeModuleTest() {
        assertSame("Good", grader.gradeModule(65));
        assertSame("Excellent", grader.gradeModule(75));
        assertSame("Excellent", grader.gradeModule(85));
        assertSame("Excellent", grader.gradeModule(99));
        assertSame("Satisfactory", grader.gradeModule(50));
        assertSame("Compensatable Fail", grader.gradeModule(49));
        assertSame("Outright Fail", grader.gradeModule(39));
    };

    @Test
    public void validateInputTest() {
        assertTrue(grader.validateInput(0));
        assertTrue(grader.validateInput(5));
        assertTrue(grader.validateInput(87));
        assertTrue(grader.validateInput(100));
        assertFalse(grader.validateInput(-5));
        assertFalse(grader.validateInput(-1));
        assertFalse(grader.validateInput(101));
    }

    @Test
    public void validateInputYesOrNo() {
        assertTrue(grader.validateInputYesOrNo("Y"));
        assertTrue(grader.validateInputYesOrNo("N"));
        assertTrue(grader.validateInputYesOrNo("n"));
        assertTrue(grader.validateInputYesOrNo("N"));

        assertFalse(grader.validateInputYesOrNo("Nn"));
        assertFalse(grader.validateInputYesOrNo("999"));
        assertFalse(grader.validateInputYesOrNo("yyyyy"));
    }
}
