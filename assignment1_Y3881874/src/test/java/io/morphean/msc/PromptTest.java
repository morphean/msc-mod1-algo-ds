package io.morphean.msc;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PromptTest {

    Prompt prompt;

    @Before
    public void setup() {
        prompt = new Prompt(12, "a new message");
    }

    @Test
    public void get_type() {
        assertTrue(prompt.get_type() == 12);
    }

    @Test
    public void get_message() {
        assertTrue( prompt.get_message() == "a new message");
    }
}