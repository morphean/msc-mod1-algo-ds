package io.morphean.msc;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class TestDegree {

    @Test
    public void testGetAverage() {
        Degree d = new Degree(10, 20, 30, 5);
        System.out.println(d.getAverage());
        assertTrue("getAverage(10,20,30, 5) returns 16.0", d.getAverage() == 16.0);

        Degree e = new Degree(1, 1, 1, 1);
        System.out.println(e.getAverage());
        assertTrue("getAverage(1,1,1,1) returns 1", e.getAverage() == 1.0);

    };
}
