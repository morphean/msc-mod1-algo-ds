package io.morphean.msc;

import java.util.Scanner;

public class DegreeGrader_Y3881874 {

    public static final String DISTINCTION = "Distinction";
    public static final String MERIT = "Merit";
    public static final String PASS = "Pass";

    private static final int MODULE_AVERAGE = 1;
    private static final int ISM_MODULE_AVERAGE = 2;
    private static final int COMPENSATABLE_FAILED_CREDITS = 3;
    private static final int OUTRIGHT_FAILED_CREDITS = 4;

    public static void main( String[] args )
    {
        // Part B
        DegreeGrader_Y3881874 grader = new DegreeGrader_Y3881874();
        grader.startDegreeGrading();
    }

    public String gradeDegree(int moduleAverage,
                              int ismAverage,
                              int cFailedCredits,
                              int oFailedModules) {
        Degree degree = new Degree(moduleAverage, ismAverage, cFailedCredits, oFailedModules);
        String result = getDegreeGrade(degree);

        return result;
    }

    private void startDegreeGrading() {
        System.out.println("*********** Degree Grading Program **********");
        System.out.println("");
        Scanner sc = new Scanner(System.in);

        boolean capturingInput = false;
        int[] results = { 0,0,0,0 };
        Prompt[] prompts = {
                new Prompt(MODULE_AVERAGE, "Overall module average (0-100): "),
                new Prompt(ISM_MODULE_AVERAGE, "ISM module average (0-100): "),
                new Prompt(COMPENSATABLE_FAILED_CREDITS, "Compensatable failed credits (0-180): "),
                new Prompt(OUTRIGHT_FAILED_CREDITS, "Outright failed modules (0-11): "),
        };

        int result;

        for (int i = 0; i < results.length; i++) {
            do {
                result = captureUserInput(prompts[i]);
                if (result > -1) {
                    results[i] = result;
                    System.out.println("Mark accepted");
                    capturingInput = true;
                }
            } while (!capturingInput);
        }

        System.out.println("");
        System.out.println("*********** Marks Recorded ******************");
        System.out.println("");

        int i = 0;
        while (i < results.length) {
            String markType = "";
            switch (i) {
                case 0:
                    markType = "Overall Module Average";
                    break;
                case 1:
                    markType = "ISM Module Average";
                    break;
                case 2:
                    markType = "Compensatable Failed Credits";
                    break;
                case 3:
                    markType = "Outright Failed Modules";
                    break;
            }
            System.out.println(markType+" recorded: "+results[i]);
            i++;
        }

        System.out.println("");
        System.out.println("*********** Processing detailed entered *****");
        System.out.println("");

        Degree degree = new Degree(results[0], results[1], results[2], results[3]);
        System.out.println("Result is: "+getDegreeGrade(degree));

    }

    private String getDegreeGrade(Degree degree) {

        if (isDistinction(degree)) {
            return "Distinction";
        }

        if (isMerit(degree)) {
            return "Merit";
        }

        if (isPass(degree)) {
            return "Pass";
        }

        return "Fail";
    }

    protected boolean isDistinction(Degree degree) {
        // candidate must score:
        // overallAverage > 70
        // independentStudyAverage > 70
        // cFailedCredits = 0
        // cFailedModules = 0;

        boolean result = false;

        if (degree.getModuleAverage() >= 70) {
            if (degree.getIsmModuleAverage() >= 70) {
                if (degree.getCompensatableFailedCredits() == 0) {
                    if (degree.getOutrightFailedModules() == 0) {
                        result = true;
                    }
                }
            }
        }

        return result;
    }

    protected boolean isMerit(Degree degree) {
        // candidate must score:
        // overallAverage > 60
        // independentStudyAverage > 60
        // cFailedCredits <= 15
        // cFailedModules = 0;

        boolean result = false;

        if (degree.getModuleAverage() >= 60) {
            if (degree.getIsmModuleAverage() >= 60) {
                if (degree.getCompensatableFailedCredits() <= 15) {
                    if (degree.getOutrightFailedModules() == 0) {
                        result = true;
                    }
                }
            }
        }

        return result;
    }

    protected boolean isPass(Degree degree) {
        // candidate must score:
        // overallAverage > 50
        // independentStudyAverage > 50
        // cFailedCredits <= 30
        // cFailedModules = 0;

        boolean result = false;

        if (degree.getModuleAverage() >= 50) {
            if (degree.getIsmModuleAverage() >= 50) {
                if (degree.getCompensatableFailedCredits() <= 30) {
                    if (degree.getOutrightFailedModules() == 0) {
                        result = true;
                    }
                }
            }
        }

        return result;
    }

    protected boolean isFail(Degree degree) {
        // candidate must score:
        // overallAverage > 50
        // independentStudyAverage > 50
        // cFailedCredits <= 30
        // cFailedModules = 0;

        boolean result = false;

        if (degree.getModuleAverage() < 50) {
            if (degree.getIsmModuleAverage() < 50) {
                if (degree.getCompensatableFailedCredits() <= 60) {
                    if (degree.getOutrightFailedModules() <= 2) {
                        result = true;
                    }
                }
            }
        }

        return result;
    }

    protected boolean validateModuleAverage(int result) {
        return result <= 100 && result >= 0;
    }

    protected boolean validateCompensatableFailedCredits(int result) {
        return result <= 180 && result >= 0;
    }

    protected boolean validateNumberOfFailedModules(int result) {
        return result <= 11 && result >= 0;
    }

    protected int captureUserInput(Prompt prompt) {
        Scanner sc = new Scanner(System.in);
        int userInput;
        boolean isValidInput = false;

        do {
            System.out.println("Please enter "+prompt.get_message());
            userInput = sc.nextInt();
            switch (prompt.get_type()) {
                case MODULE_AVERAGE:
                case ISM_MODULE_AVERAGE:
                    isValidInput = validateModuleAverage(userInput);
                    break;
                case OUTRIGHT_FAILED_CREDITS:
                    isValidInput = validateNumberOfFailedModules(userInput);
                    break;
                case COMPENSATABLE_FAILED_CREDITS:
                    isValidInput = validateCompensatableFailedCredits(userInput);
                    break;
                default:
                    break;
            }

            if (!isValidInput) {
                System.out.println("ERROR: Please enter a valid mark.");
            }

        } while (!isValidInput);

        return userInput;
    }

}
