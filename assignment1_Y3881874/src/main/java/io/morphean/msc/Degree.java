package io.morphean.msc;

public class Degree {
    private int _moduleAverage;

    public int getModuleAverage() {
        return _moduleAverage;
    }

    private int _ismModuleAverage;

    public int getIsmModuleAverage() {
        return _ismModuleAverage;
    }

    private int _compensatableFailedCredits;

    public int getCompensatableFailedCredits() {
        return _compensatableFailedCredits;
    }

    private int _outrightFailedModules;

    public int getOutrightFailedModules() {
        return _outrightFailedModules;
    }


    public Degree(int mAverage,
                  int ismAverage,
                  int cFailedCredits,
                  int oFailedModules) {
        _moduleAverage = mAverage;
        _ismModuleAverage = ismAverage;
        _compensatableFailedCredits = cFailedCredits;
        _outrightFailedModules = oFailedModules;
    }

    public double getAverage() {
        return (_moduleAverage+_ismModuleAverage+_compensatableFailedCredits+_outrightFailedModules) / 4;
    }

    @Override
    public String toString() {
        return "Degree{" +
                "_moduleAverage=" + _moduleAverage +
                ", _ismModuleAverage=" + _ismModuleAverage +
                ", _compensatableFailedCredits=" + _compensatableFailedCredits +
                ", _outrightFailedModules=" + _outrightFailedModules +
                '}';
    }

}
