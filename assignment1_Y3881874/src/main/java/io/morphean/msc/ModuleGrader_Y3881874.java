package io.morphean.msc;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ModuleGrader_Y3881874 {

    public static void main( String[] args )
    {
        // Part A
        ModuleGrader_Y3881874 grader = new ModuleGrader_Y3881874();
        grader.startModuleGrading();
    }

    public String gradeModule(int mark) {
        int outrightFail = 39;
        int compensatableFail = 40;
        int satisfactory = 50;
        int good = 60;
        int excellent = 70;

        if (mark >= 0 && mark <= outrightFail) {
            return "Outright Fail";
        }

        if (mark >= compensatableFail && mark < satisfactory) {
            return "Compensatable Fail";
        }

        if (mark >= satisfactory && mark < good) {
            return "Satisfactory";
        }

        if (mark >= good && mark < excellent) {
            return "Good";
        }

        // these could be split out to getter functions, ie:  boolean isExcellent(mark)
        if (mark >= excellent) {
            return "Excellent";
        }

        return "Fail";
    }
    // take input from user and continue
    private int getValidModuleMark() {
        //Create a new scanner object to receive input from the keyboard
        Scanner sc = new Scanner(System.in);
        int gradeEntered;
        boolean validInput;

        do {
            System.out.println("Please enter mark (0-100): ");
            gradeEntered = sc.nextInt();
            validInput = validateInput(gradeEntered);
            if (validInput) {
                return gradeEntered;
            }
            else {
                System.out.println("ERROR: Please enter a number in the range 0 to 100");
            }
        } while (!validInput);

        return -1;
    }

    private boolean doesUserWantToContinue() {
        Scanner sc = new Scanner(System.in);
        boolean captureInput = false;

            System.out.println("Would you like to grade another? (y/n): ");
            String choice = sc.next();

            if (validateInputYesOrNo(choice)) {
                switch (choice.toLowerCase()) {
                    case "y":
                        captureInput = true;
                        break;
                    case "n":
                        System.out.println("**** Thank you for using this service... **** ");
                        captureInput = false;
                        sc.close();
                    default:
                        break;
                }
            }
            else {
                System.out.println("**** ERROR: Valid input is 'y' or 'n' **** ");
                captureInput = true;
            }

        return captureInput;
    }

    // check input is in the valid range between 0 and 100;
    protected boolean validateInput(int input) {
        return input >= 0 && input <= 100;
    }

    // check input is in the valid range between 0 and 100;
    protected boolean validateInputYesOrNo(String input) {
        String choiceLC = input.toLowerCase();
        String regex = "^(y|n)$";

        // an alternative
        //   Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        //   Matcher matcher = pattern.matcher(choiceLC);
        //   matcher.matches(choiceLC);

        return choiceLC.matches(regex);
    }

    public void startModuleGrading() {
        ArrayList<Integer> results = new ArrayList();
        int result;
        boolean continueGrading = true;

        System.out.println("*********** Module Grading Program **********");
        System.out.println("");

        do {
            if (continueGrading) {
                result = getValidModuleMark();
                if (result > -1) {
                    results.add(result);
                    System.out.println("Mark accepted");
                    continueGrading = doesUserWantToContinue();
                }
            }
        } while (continueGrading);

        System.out.println("");
        System.out.println("*********** Marks Recorded ******************");
        System.out.println("");

        int i = 0;
        while (i < results.size()) {
            System.out.println("Mark "+(i+1)+" recorded: "+results.get(i));
            i++;
        }
    }
}
