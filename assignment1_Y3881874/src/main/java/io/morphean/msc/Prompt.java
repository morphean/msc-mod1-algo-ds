package io.morphean.msc;

public class Prompt {
    private int _type;

    public int get_type() {
        return _type;
    }

    private String _message;

    public String get_message() {
        return _message;
    }

    public Prompt(int type, String message) {
        _type = type;
        _message = message;
    }
}
